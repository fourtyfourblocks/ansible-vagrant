#!/usr/bin/env bash

# no pass phrase
# ssh-keygen -t rsa -N "" -f ~/.ssh/id_rsa
#ssh -oStrictHostKeyChecking=no

# variables
VAGRANT_USER=vagrant
ANSIBLE_HOME=/vagrant/ansible
VAGRANT_HOME=/vagrant

echo 'starting configuration'

sudo apt-get --yes update > /dev/null
sudo apt-get --yes install vim > /dev/null

# ansible
sudo apt-get --yes install python-software-properties > /dev/null
sudo apt-add-repository --yes ppa:rquillo/ansible > /dev/null
sudo apt-get --yes install ansible > /dev/null
sudo apt-get --yes update > /dev/null

echo 'copying ansible configuration'
cp $ANSIBLE_HOME/hosts /etc/ansible/
cp $ANSIBLE_HOME/id_rsa* /home/$VAGRANT_USER/.ssh/
cp $ANSIBLE_HOME/.ansible.cfg /home/$VAGRANT_USER/

# rights
echo 'setting rights'
sudo chown -R vagrant:vagrant /etc/ansible/
sudo chmod -R ug+rw /etc/ansible/
sudo chmod -R o=r /etc/ansible/

sudo chown vagrant:vagrant /home/$VAGRANT_USER/.ansible.cfg
sudo chmod ug+rw /home/$VAGRANT_USER/.ansible.cfg
sudo chmod o=r /home/$VAGRANT_USER/.ansible.cfg

sudo chown -R vagrant:vagrant /home/$VAGRANT_USER/.ssh/
sudo chmod -R u+rw /home/$VAGRANT_USER/.ssh/id_rsa*
sudo chmod -R go-rwx /home/$VAGRANT_USER/.ssh/id_rsa*

# ssh
ssh-agent bash
ssh-add /home/$VAGRANT_USER/.ssh/id_rsa

# ansible pi -m ping -u pi